# MiscStuffICameAcrossOrMadeAndDontWanttoLose
A collection of stuff I came across or made and don't want to lose

Everything is licenced under:

[![Licence](https://gitlab.com/Unrepentant-Atheist/MiscStuffICameAcrossOrMadeAndDontWanttoLose/raw/f2891a757f24de42c45a970e5a3a2715a9187c2f/CC_licence.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/)